class ResourceSerializer
  include FastJsonapi::ObjectSerializer
  set_type :resource

  attributes :name, :value
end
