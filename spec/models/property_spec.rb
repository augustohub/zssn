require 'rails_helper'

RSpec.describe Property, type: :model do
  it 'is not valid without a owner survivor' do
    expect(Property.new(survivor_id: nil)).not_to be_valid
  end

  it 'is not valid without a resource' do
    expect(Property.new(resource_id: nil)).not_to be_valid
  end
end
