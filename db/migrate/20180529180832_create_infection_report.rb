class CreateInfectionReport < ActiveRecord::Migration[5.2]
  def change
    create_table :infection_reports do |t|
      t.references :accuser, index: true, foreign_key: {to_table: :survivors}
      t.references :infected, index: true, foreign_key: {to_table: :survivors}
    end
  end
end
