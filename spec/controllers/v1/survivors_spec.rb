require 'rails_helper'

describe Api::V1::SurvivorsController, type: :controller do

  let(:infected_person)     { create(:survivor, :infected) }
  let(:non_infected_person) { create(:survivor, :non_infected) }

  before do
    infected_person
    non_infected_person
  end

  describe 'GET #index' do
    it 'request index and return 200 OK' do
      get :index
      expect(response).to have_http_status(:ok)
    end

    it 'lists only non-infected survivors' do
      expect(Survivor.non_infected).not_to include([infected_person])
    end
  end

  describe 'GET #show' do
    it 'request show and return 200 Ok' do
      get :show, params: { id: non_infected_person.id }
      response_body = JSON.parse(response.body)
      expect(response_body['data'].fetch('id').to_i).to eq(non_infected_person.id)
    end
  end
end
