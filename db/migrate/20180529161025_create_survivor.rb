class CreateSurvivor < ActiveRecord::Migration[5.2]
  def change
    create_table :survivors do |t|
      t.string  :name
      t.integer :age
      t.integer :gender   , default: 0
      t.decimal :latitude , precision: 10, scale: 6
      t.decimal :longitude, precision: 10, scale: 6
    end
  end
end
