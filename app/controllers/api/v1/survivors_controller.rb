module Api::V1
  class SurvivorsController < ApiController
    before_action :set_survivor, only: %i[show update]

    # GET /api/v1/survivors
    def index
      @survivors = Survivor.non_infected
      render json: serialize(@survivors)
    end

    # GET /api/v1/survivors/1
    def show
      render json: serialize(@survivor)
    end

    # POST /api/v1/survivors
    def create
      @survivor = Survivor.new(survivor_params)
      if @survivor.save
        render json: serialize(@survivor), status: :created
      else
        render json: @survivor.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/v1/survivors/1
    def update
      if @survivor.update(survivor_params)
        render json: serialize(@survivor)
      else
        render json: @survivor.errors, status: :unprocessable_entity
      end
    end

    private

    def serialize(object)
      SurvivorSerializer.new(object).serialized_json
    end

    def set_survivor
      @survivor = Survivor.find(params[:id])
    end

    def survivor_params
      params.require(:survivor)
            .permit(:name, :age, :gender, :latitude, :longitude,
                    properties_attributes: [:resource_id] )
    end
  end
end
