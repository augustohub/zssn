class PropertySerializer
  include FastJsonapi::ObjectSerializer
  set_type :property
  belongs_to :survivor
  belongs_to :resource
end
