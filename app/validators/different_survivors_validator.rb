class DifferentSurvivorsValidator < ActiveModel::Validator
  def validate(record)
    if record.survivor1.id == record.survivor2.id
      record.errors.add(:base,
                        'A survivor cannot trade items with himself')
    end
  end
end
