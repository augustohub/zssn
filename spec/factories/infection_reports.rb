FactoryBot.define do
  factory :infection_report do
    association :accuser,  factory: :survivor
    association :infected, factory: :survivor
  end
end
