class TradingService
  include ActiveModel::Validations
  include ActiveModel::Callbacks

  define_model_callbacks :initialize, only: [:after]
  after_initialize :valid?

  attr_reader :survivor1, :survivor2, :properties1, :properties2

  validates_with PropertiesAvailabilityValidator
  validates_with EquivalentTradeValidator
  validates_with DifferentSurvivorsValidator
  validates_with NotInfectedTradersValidator
  validates :survivor1, presence: true
  validates :survivor2, presence: true
  validates :properties1, presence: true
  validates :properties2, presence: true

  def initialize(trade_params)
    run_callbacks :initialize do
      offer1_params = trade_params[:offer1]
      offer2_params = trade_params[:offer2]
      @survivor1 = fetch_survivor(offer1_params[:survivor_id])
      @survivor2 = fetch_survivor(offer2_params[:survivor_id])
      @properties1 = fetch_properties(offer1_params[:properties_ids])
      @properties2 = fetch_properties(offer2_params[:properties_ids])
    end
  end

  def self.run(trade_params)
    self.new(trade_params).tap do |service|
      service.change_ownership unless service.errors.any?
    end
  end

  def change_ownership
    @properties1.each { |p| p.update_attribute(:survivor_id, @survivor2.id) }
    @properties2.each { |p| p.update_attribute(:survivor_id, @survivor1.id)}
  end

  private

  def fetch_survivor(survivor_id)
    Survivor.find(survivor_id)
  end

  def fetch_properties(properties_ids)
    Property.where(id: properties_ids)
  end
end
