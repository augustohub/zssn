module Api::V1
  class ResourcesController < ApiController
    before_action :set_resource, only: %i[show update]

    # GET /api/v1/resources
    def index
      @resources = Resource.all
      render json: serialize(@resources)
    end

    # GET /api/v1/resources/1
    def show
      render json: serialize(@resource)
    end

    # POST /api/v1/resources
    def create
      @resource = Resource.new(resource_params)
      if @resource.save
        render json: serialize(@resource, status: :created)
      else
        render json: @resource.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/v1/resources/1
    def update
      if @resource.update(resource_params)
        render json: serialize(@resource)
      else
        render json: @resource.errors, status: :unprocessable_entity
      end
    end

    private

    def set_resource
      @resource = Resource.find(params[:id])
    end

    def serialize(object)
      ResourceSerializer.new(object).serialized_json
    end

    def resource_params
      params.require(:resource).permit(:name, :value)
    end
  end
end
