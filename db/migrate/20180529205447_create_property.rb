class CreateProperty < ActiveRecord::Migration[5.2]
  def change
    create_table :properties do |t|
      t.references :survivor, index: true, foreign_key: true
      t.references :resource, index: true, foreign_key: true
    end
  end
end
