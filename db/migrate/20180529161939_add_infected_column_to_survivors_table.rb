class AddInfectedColumnToSurvivorsTable < ActiveRecord::Migration[5.2]
  def change
    add_column :survivors, :infected, :boolean, default: false
  end
end
