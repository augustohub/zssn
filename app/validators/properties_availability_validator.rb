class PropertiesAvailabilityValidator < ActiveModel::Validator
  def validate(record)
    unless properties_available?(record.survivor1, record.properties1) &&
           properties_available?(record.survivor1, record.properties2)

      record.errors.add(:base,
                        'Some items in this trade are not available')
    end
  end

  private

  def properties_available?(survivor, properties)
    (properties - survivor.properties.map(&:id)).empty?
  end
end
