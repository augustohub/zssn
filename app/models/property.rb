# Table name: properties
#
# id           :integer, not null, primary key
# survivor_id  :integer
# resource_id  :integer
#

class Property < ApplicationRecord
  validates :survivor, :resource, presence: true

  belongs_to :survivor
  belongs_to :resource

  scope :valid, -> { joins(:survivor).where('survivors.infected = 0') }
  scope :lost,  -> { joins(:survivor).where('survivors.infected = 1') }

  def is_valid?
    !survivor.infected?
  end
end
