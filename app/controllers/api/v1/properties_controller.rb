module Api::V1
  class PropertiesController < ApiController

    def index
      @properties = Survivor.find(params[:survivor_id]).properties
      render json: PropertySerializer.new(@properties).serialized_json
    end
  end
end
