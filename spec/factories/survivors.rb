require 'faker'

FactoryBot.define do
  factory :survivor, aliases: [:accuser, :accused] do
    name      Faker::Name.name
    age       35
    gender    'not_sure'
    latitude  10.000000
    longitude 10.000000

    trait :infected do
      infected true
    end

    trait :non_infected do
      infected false
    end

    trait :male do
      gender 'male'
    end

    trait :female do
      gender 'female'
    end

    trait :undifined_gender do
      gender 'not_sure'
    end
  end
end
