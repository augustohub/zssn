Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :survivors, only: %i[index show create update] do
        get :inventory, to: 'properties#index'
      end
      resources :resources, only: %i[index show create update]
      post :report_infection, to: 'infection_reports#create'

      post :trade, to: 'trades#create'

      scope '/reports' do
        get :infected_percentage,     to: 'reports#infected_percentage'
        get :non_infected_percentage, to: 'reports#non_infected_percentage'
        get :average_resources,       to: 'reports#average_resources'
        get :points_lost,             to: 'reports#points_lost'
      end
    end
  end
end
