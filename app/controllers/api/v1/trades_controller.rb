module Api::V1
  class TradesController < ApiController
    def create
      @trade = TradingService.run(trade_params)
      if @trade.valid?
        render status: :created
      else
        render json: @trade.errors, status: :unprocessable_entity
      end
    end

    private

    def trade_params
      params.require(:trade)
            .permit(offer1: [:survivor_id, properties_ids: []],
                    offer2: [:survivor_id, properties_ids: []])
    end
  end
end
