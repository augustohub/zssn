# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_05_29_205447) do

  create_table "infection_reports", force: :cascade do |t|
    t.integer "accuser_id"
    t.integer "infected_id"
    t.index ["accuser_id"], name: "index_infection_reports_on_accuser_id"
    t.index ["infected_id"], name: "index_infection_reports_on_infected_id"
  end

  create_table "properties", force: :cascade do |t|
    t.integer "survivor_id"
    t.integer "resource_id"
    t.index ["resource_id"], name: "index_properties_on_resource_id"
    t.index ["survivor_id"], name: "index_properties_on_survivor_id"
  end

  create_table "resources", force: :cascade do |t|
    t.string "name"
    t.integer "value"
  end

  create_table "survivors", force: :cascade do |t|
    t.string "name"
    t.integer "age"
    t.integer "gender", default: 0
    t.decimal "latitude", precision: 10, scale: 6
    t.decimal "longitude", precision: 10, scale: 6
    t.boolean "infected", default: false
    t.integer "infection_reports_count", default: 0, null: false
  end

end
