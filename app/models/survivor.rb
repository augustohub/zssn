# Table name: survivors
#
# id          :integer, not null, primary key
# name        :string
# age         :integer
# gender      :integer
# latitude    :decimal, precision: 10, scale: 6
# longitude   :decimal, precision: 10, scale: 6
# infected    :boolean, default: false
#


class Survivor < ApplicationRecord

  enum gender: { not_sure: 0, male: 1, female: 2 }

  validates :name, :gender, :latitude, :longitude, presence: true

  has_many :infection_reports,
           class_name: 'InfectionReport',
           foreign_key: :infected_id

  has_many :infection_accusations,
           class_name: 'InfectionReport',
           foreign_key: :accuser_id

  has_many :properties
  has_many :resources, through: :properties

  accepts_nested_attributes_for :properties

  scope :infected,     -> { where(infected: true)}
  scope :non_infected, -> { where(infected: false)}

  def check_for_infection
    infected! if !infected && infection_reports.count >= 3
  end

  def infected!
    update_column(:infected, true)
  end
end
