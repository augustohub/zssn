# Introduction
The world as we know it has fallen into an apocalyptic scenario. A laboratory-made virus is transforming human beings and animals into zombies, hungry for fresh flesh.

The human kind must resist! We do not just need strength and cool weapons, but also organization...

So, a nerd guy developed a system to manage resources and trades beetween non-infected humans.

# Overview
This is a REST API developed with Rails 5 (that doesn't ends on Terminus) and SQLite, that is able to:
 - Create and manage survivors;
 - Keep control survivors inentory;
 - Manage trades between survivors;
 - Let survivors mark someone as infected;
 - Report a survivor as infected;
 - Give reports about your community of survivors;
 - Create and Catalog resources.

# API Documentation
Api documentation can be found on the following link:
https://documenter.getpostman.com/view/1907334/zssn-zombie-survival-social-network/RW8CJTaS
