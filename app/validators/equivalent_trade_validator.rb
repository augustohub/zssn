class EquivalentTradeValidator < ActiveModel::Validator
  def validate(record)
    unless equivalent_value?(record)
      record.errors.add(:base,
                        'The items on this trade does ' \
                        'not have equivalent values')
    end
  end

  private

  def equivalent_value?(record)
    resource_values(record.properties1) == resource_values(record.properties2)
  end

  def resource_values(properties)
    properties.sum { |p| p.resource.value }
  end
end
