require 'rails_helper'

RSpec.describe InfectionReport, type: :model do

  it 'is not valid without a accuser' do
    expect(InfectionReport.new(accuser_id: nil)).not_to be_valid
  end

  it 'is not valid without a accused' do
    expect(InfectionReport.new(infected_id: nil)).not_to be_valid
  end

  let(:infected_accuser) { build(:accuser, :infected) }
  
  it 'is not valid if accuser is infected' do
    expect(build(:infection_report, accuser: infected_accuser)).not_to be_valid
  end
end
