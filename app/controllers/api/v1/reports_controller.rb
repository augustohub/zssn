module Api::V1
  class ReportsController < ApiController

    # GET /api/v1/report/infected_percentage
    def infected_percentage
      survivors = Survivor.all.count.to_f
      infected  = Survivor.infected.count.to_f
      infected_percentage = ((infected / survivors) * 100).to_s + '%'
      @report = { description: 'Percentage of infected survivors',
                  result: infected_percentage }
      render json: @report
    end

    # GET /api/v1/report/non_infected_percentage
    def non_infected_percentage
      survivors = Survivor.all.count.to_f
      non_infected  = Survivor.non_infected.count.to_f
      non_infected_percentage = ((non_infected / survivors) * 100).to_s + '%'
      @report = { description: 'Percentage of non infected survivors',
                  result: non_infected_percentage }
      render json: @report
    end

    # GET /api/v1/report/average_resources
    def average_resources
      survivors = Survivor.all.count.to_f
      resources_avg = {}
      resources = Property.joins(:resource).valid.group('resources.name').count

      resources.each { |k, v| resources_avg[k] = v / survivors }
      @report = { description: 'Average amount of each kind of resource by ' \
                               'survivor (e.g. 5 waters per survivor)',
                  result: resources_avg }
      render json: @report
    end

    # GET /api/v1/report/points_lost
    def points_lost
      lost_resources = Property.lost.to_a
      lost_points = lost_resources.sum { |prop| prop.resource.value }
      @report = { description: 'Points lost because of infected survivor.',
                  result: lost_points }
      render json: @report
    end
  end
end
