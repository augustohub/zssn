class AddInfectionReportsCountToSurvivor < ActiveRecord::Migration[5.2]
  def change
    add_column :survivors,
               :infection_reports_count,
               :integer,
               default: 0,
               null: false
  end
end
