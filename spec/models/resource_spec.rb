require 'rails_helper'

RSpec.describe Resource, type: :model do
  it 'is not valid without a name' do
    expect(build(:resource, name: nil)).not_to be_valid
  end

  it 'is not valid without value' do
    expect(build(:resource, value: nil)).not_to be_valid
  end
end
