require 'rails_helper'

RSpec.describe Survivor, type: :model do
  it 'is not valid without a name' do
    expect(build(:survivor, name: nil)).not_to be_valid
  end

  it 'is not valid without gender' do
    expect(build(:survivor, gender: nil)).not_to be_valid
  end

  it 'is not valid without a longitude' do
    expect(build(:survivor, longitude: nil)).not_to be_valid
  end

  it 'is not valid without a latitude' do
    expect(build(:survivor, latitude: nil)).not_to be_valid
  end

  describe 'is considred as infected?' do
    let(:accuser) { create(:survivor, :non_infected) }
    let(:accused) { create(:survivor, :non_infected) }

    before do
      3.times do
        InfectionReport.create(accuser: accuser, infected: accused)
      end
    end

    context 'has 3 accusations' do
      it 'is considered as infected' do
        expect(accused.infected).to be true
      end
    end
  end
end
