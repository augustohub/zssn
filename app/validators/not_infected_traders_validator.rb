class NotInfectedTradersValidator < ActiveModel::Validator
  def validate(record)
    if record.survivor1.infected || record.survivor2.infected
      record.errors.add(:base,
                        'A Survivor cannot trade with an infected person')
    end
  end
end
