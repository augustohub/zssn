# Table name: resources
#
# id    :integer, not null, primary key
# name  :string
# value :integer
#

class Resource < ApplicationRecord
  validates :name, :value, presence: true

  has_many :properties
  has_many :survivors, through: :properties
end
