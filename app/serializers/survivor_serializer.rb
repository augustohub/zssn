class SurvivorSerializer
  include FastJsonapi::ObjectSerializer
  set_type :survivor

  attributes :name, :age, :gender, :infected

  has_many :properties
  has_many :resources, through: :properties

  attribute :last_location do |object|
    "#{object.latitude}, #{object.longitude}"
  end
end
