module Api::V1
  class InfectionReportsController < ApiController

    # POST /api/v1/report_infection
    def create
      @infection_report = InfectionReport.new(infection_report_params)
      if @infection_report.save
        render json: @infection_report, status: :created
      else
        render json: @infection_report.errors, status: :unprocessable_entity
      end
    end

    private

    def infection_report_params
      params.require(:infection_report).permit(:accuser_id, :infected_id)
    end
  end
end
