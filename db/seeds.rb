require 'faker'

puts "Creating Resources"
INITIAL_RESOURCES = [
  ['Water', 4],
  ['Food', 3],
  ['Medication', 2],
  ['Ammunition', 1]
]

INITIAL_RESOURCES.each do |name, value|
  Resource.create( name: name, value: value )
end


def random_properties_array
  properties = []
  rand(0..5).times do
    properties << { resource_id: rand(1..INITIAL_RESOURCES.length) }
  end

  properties
end

puts "Creating Survivors"
5.times do
  Survivor.create name:      Faker::Name.name,
                  age:       rand(14..80),
                  gender:    rand(0..2),
                  latitude:  10.000000,
                  longitude: 10.000000,
                  properties_attributes: random_properties_array
end
