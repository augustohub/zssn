# Table name: infection_reports
#
# id          :integer, not null, primary key
# infected_id :integer
# accuser_id  :integer

class InfectionReport < ApplicationRecord

  validates :infected_id, :accuser_id, presence: true

  before_create :valid_accusation?
  after_save    :check_accused_for_infection

  belongs_to :infected,
             class_name: 'Survivor',
             foreign_key: :infected_id,
             counter_cache: true

  belongs_to :accuser,
             class_name: 'Survivor',
             foreign_key: :accuser_id

  def check_accused_for_infection
    infected.check_for_infection
  end

  def valid_accusation?
    return true unless accuser.infected
  end
end
